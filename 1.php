<?php
//Creamos un objeto para hacer la conexión con la base de datos

$conexion = new mysqli(
    '127.0.0.1',  // servidor de base de datos
    'root', // usuario
    '', // contraseña
    'desarrollo', // nombre de la base de datos
    3306, //puerto
);

// Realizamos una consulta (sacar los alumnos de la tabla alumnos)

$resultado = $conexion->query("select * from alumnos");
var_dump($resultado);

// Guardamos en fila un array con los campos y sus valores del primer registro
$fila = $resultado->fetch_assoc();
var_dump($fila);
// Leo el segundo registro de la tabla
$fila = $resultado->fetch_assoc();
var_dump($fila);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <table border="1" style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>

            <tr>
                <td> <?= $fila["codigo"] ?></td>
                <td> <?= $fila["nombre"] ?></td>
                <td> <?= $fila["correo"] ?></td>
            </tr>


        </tbody>
    </table>

</body>

</html>