<?php
// Cargar un fichero externo ( en este caso una libreria que contiene las funciones que tiene las consultas de SQL)
require_once "conexion.inc";

$conexion = conectar();

// $consultaAlumnos($conexion, "select * from alumnos");

// $consultaExamenes($conexion, "select * from examenes");

$registrosAlumnos = alumnos($conexion);
$registrosExamenes = examenes($conexion);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "_alumnos.php";
    require_once "_examenes.php";
    ?>
</body>

</html>