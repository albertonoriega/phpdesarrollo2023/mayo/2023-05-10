<?php
//Creamos un objeto para hacer la conexión con la base de datos

$conexion = new mysqli(
    '127.0.0.1',  // servidor de base de datos
    'root', // usuario
    '', // contraseña
    'desarrollo', // nombre de la base de datos
    3306, //puerto
);

$resultado = $conexion->query("select * from alumnos");
var_dump($resultado);

// Con fetch->array() nos da por defecto un array que es tanto enumerado y asociativo
/*
0 => string '1' (length=1)
'codigo' => string '1' (length=1)
1 => string 'Eva' (length=3)
'nombre' => string 'Eva' (length=3)
2 => string 'eva@alpe.es' (length=11)
'correo' => string 'eva@alpe.es' (length=11)
*/
$fila = $resultado->fetch_array();
var_dump($fila);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>

    <table border="1" style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>

            <tr>
                <td> <?= $fila["codigo"] ?></td>
                <td> <?= $fila["nombre"] ?></td>
                <td> <?= $fila["correo"] ?></td>
            </tr>
            <tr>
                <td> <?= $fila[0] ?></td>
                <td> <?= $fila[1] ?></td>
                <td> <?= $fila[2] ?></td>
            </tr>


        </tbody>
    </table>

</body>

</html>