<?php

function conectar()
{
    $conexion = new mysqli(
        '127.0.0.1',  // servidor de base de datos
        'root', // usuario
        '', // contraseña
        'desarrollo', // nombre de la base de datos
        3306, //puerto
    );
    return $conexion;
}


function consulta($conexion, $consulta)
{
    return $conexion->query($consulta);
}

function alumnos($conexion)
{
    $ConsultaAlumnos = $conexion->query("select * from alumnos");
    return $ConsultaAlumnos->fetch_all(MYSQLI_ASSOC);
}

function examenes($conexion)
{
    $ConsultaExamenes = $conexion->query("select * from examenes");
    return $ConsultaExamenes->fetch_all(MYSQLI_ASSOC);
}
