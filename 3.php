<?php
//Creamos un objeto para hacer la conexión con la base de datos

$conexion = new mysqli(
    '127.0.0.1',  // servidor de base de datos
    'root', // usuario
    '', // contraseña
    'desarrollo', // nombre de la base de datos
    3306, //puerto
);


$conexionAlumnos = $conexion->query("select * from alumnos");
var_dump($conexionAlumnos);

// Guadamos todos los registros en un array de arrays
// Le ponemos MYSQLI_ASSOC para que nos devuelva un array de arrays asociativos
$registrosAlumnos = $conexionAlumnos->fetch_all(MYSQLI_ASSOC);
var_dump($registrosAlumnos);

$conexionExamenes = $conexion->query("select * from examenes");

$registrosExamenes = $conexionExamenes->fetch_all(MYSQLI_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <p>Tabla con los registros de alumnos</p>
    <table border="1" style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>Código</td>
                <td>Nombre</td>
                <td>Correo</td>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($registrosAlumnos); $i++) {
            ?>
                <tr>
                    <td> <?= $registrosAlumnos[$i]["codigo"] ?></td>
                    <td> <?= $registrosAlumnos[$i]["nombre"] ?></td>
                    <td> <?= $registrosAlumnos[$i]["correo"] ?></td>
                </tr>
            <?php
            }
            ?>

        </tbody>
    </table>

    <p>Tabla con los registros de examenes</p>
    <table border="1" style="text-align: center;">
        <thead style="background-color: #ccc;">
            <tr>
                <td>ID</td>
                <td>Titulo</td>
                <td>Nota</td>
                <td>Fecha</td>
                <td>Código Alumno</td>
            </tr>
        </thead>
        <tbody>
            <?php
            for ($i = 0; $i < count($registrosExamenes); $i++) {
            ?>
                <tr>
                    <td> <?= $registrosExamenes[$i]["id"] ?></td>
                    <td> <?= $registrosExamenes[$i]["titulo"] ?></td>
                    <td> <?= $registrosExamenes[$i]["nota"] ?></td>
                    <td> <?= $registrosExamenes[$i]["fecha"] ?></td>
                    <td> <?= $registrosExamenes[$i]["codigoAlumno"] ?></td>

                </tr>
            <?php
            }
            ?>

        </tbody>
    </table>
</body>

</html>